const button = document.getElementById("enter");
const input = document.getElementById("userInput");
const ul = document.querySelector("ul");
const dropdown = document.querySelector("select");
const li = ul.getElementsByTagName('li');
//const butoane = document.getElementsByClassName("butoane");
// console.log(butoane[0]);
const buttonAll = document.getElementById("all");
const buttonLactate = document.getElementById("lactate");
const buttonCarne = document.getElementById("carne");
const buttonSucuri = document.getElementById("sucuri");


//const listItems= document.querySelector("ul");
//selectati drop-down-ul din html si but de stilizare

function inputLength(){
    return input.value.length;
}

function getValue(target){
    return target.value;
}

function createListElement(){
    const li = document.createElement("li");
    const span = document.createElement("span");
    const icon = document.createElement("i");

    icon.classList.add("fas", "fa-trash");
    span.className = "shopping-item-name";
    span.textContent=input.value;

    let sub=li.className= dropdown.options[dropdown.selectedIndex].value;
    li.classList.add(sub, "all");
    console.log(li);
 
    li.appendChild(span);
    li.appendChild(icon);
    ul.appendChild(li);
    input.value="";
}

function deleteListElement(target){
    //target.remove();
    ul.removeChild(target.parentElement);
}

function addListItemAfterClick(){
    if(inputLength()>0)
    {
        createListElement();
    }
}


function addListItemAfterKeypress(event){
    if(inputLength()>0 && event.key==="Enter"){
        createListElement();
    }

}

button.addEventListener("click", addListItemAfterClick);
input.addEventListener("keypress", addListItemAfterKeypress);

document.addEventListener("click", event =>{
    //console.log(event.target);

        if(event.target.classList.contains("fa-trash")){
            deleteListElement(event.target);
        }
        else if(event.target.classList.contains("shopping-item-name")){
            event.target.classList.toggle("done");
        }
        else{
            return;
        }

    
});

//functie in care sa filtrati elementele din pagina
// function fliterElementsCarne(){;
//     for (var i = 0; i < items.length; i++) {
//         console.log(items[i]);
//         if(items[i].className!="carne all"){
//            ul.removeChild(items[i]);
//         }
//     }
// }
// function fliterElementsLactate(){;
//     for (let i = 0; i < items.length; i++) {
//         console.log(items[i]);
//         if(items[i].className!="lactate all"){
//             ul.removeChild(items[i]);
//         }
//     }
// }
// function fliterElementsSucuri(){;
//     for (let i = 0; i < items.length; i++) {
//         console.log(items[i]);
//         if(items[i].className!="sucuri all"){
//             ul.removeChild(items[i]);
//         }
//     }
// }
// function fliterElementsAll(){;
//     console.log("ai intrat in filterElementAll");
//     for (let i = 0; i < items.length; i++) {
//         console.log(items[i]);
//         if(items[i].className!="carne all"){
//             ul.removeChild(items[i]);
//         }
//     }
// }

// $("button").click(function(){
//     //remove selected class from all buttons
//     $("button").removeClass("selected");
  
//     //add selected class only to clicked button
//     $(this).addClass("selected");
  
//     //get data type filter
//     var dataFilter = $(this).data('filter');
  
//     //if data filter is all show all of them
//     if(dataFilter == "all") {
//         $(".elements li").show();
//     }
//     else
//     {
//       //else hide all of them and show only the one with correct data filter
//       $(".elements li").hide();
//       $("." + dataFilter).show();
//     }
//   });


buttonCarne.addEventListener("click",myFunction1);
buttonLactate.addEventListener("click",myFunction2);
buttonSucuri.addEventListener("click",myFunction3);
buttonAll.addEventListener("click",myFunction4);

function myFunction1(){
    // Declare variables
    let i;
    //console.log(s);
    // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < li.length; i++) {
            if (li[i].className=="carne all") {
              li[i].style.display = "";
            } else {
              li[i].style.display = "none";
            }
          }
}

function myFunction2(){
    // Declare variables
    let i;
    //console.log(s);
    // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < li.length; i++) {
            if (li[i].className=="lactate all") {
              li[i].style.display = "";
            } else {
              li[i].style.display = "none";
            }
          }
    
}

function myFunction3(){
    // Declare variables
    let i;
    //console.log(s);
    // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < li.length; i++) {
            if (li[i].className == "sucuri all") {
              li[i].style.display = "";
            } else {
              li[i].style.display = "none";
            }
          }
    
}

function myFunction4(){
    // Declare variables
    let i;
    //console.log(s);
    // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < li.length; i++) {
            if(li[i].className == "sucuri all" || li[i].className == "lactate all" || li[i].className == "carne all")
                li[i].style.display = "";
            else{
                li[i].style.display="none";
            }
        }
}