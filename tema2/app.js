//console.log(window);
//console.log(document);
/*
var a=3;
var b=3.12;
console.log(a,b);

var c ="training" +3;
var d ='1' + 3;
var e=`misto ${2+3}`;

console.log(Math.floor(3.14));
console.log(Math.floor(Math.random()*100));

var varsta=22;
var anStudiu=3;
var facultate="CSIE";
var Tiberiu ="Tibi are" +varsta + " de ani si e la faculateta" + facultate;
varTibiSimplu= `Tibi are ${varsta}`;
var adevarat =true;
var negativ = false;
var da = null;
*/
// var, let, const
/*for(var i=0; i < 10; i++)
{
    console.log(i);//pot folosi si in afara for-ului
}
console.log(a); //ne atentioneaza, dar incearca sa faca operatii cu el
console.log(i);
var a=3;
var a=10;
console.log(a);

const a=3;
a=5; //te anunta
console.log("training", c);
console.log("numar", d);
console.log("e:", e);

let a = 3; //fata de var, te atentioneaza
a = 10;//also nu poti folosi variabila inainte de a fi declarata

//printr-un block inteleg ce se afla in int acoladelor
const x=20;
    if(true){
        const x=10;
        console.log(x);
        const y=20;
    }

console.log(x);
//daca pun tot codul intre acolade nu se afiseaza in inspect 


//OPERATORI
let a=10;
let b=a+3;
let c=b++;
let d=a-1;
let e=a--;
console.log("1"===1);
console.log(a, b,c,d);
console.log(3<5); // >= <=

const z=3>5;
console.log(!z);
console.log((3<5 && 2>5)||true);

console.log(3<5||2>5);


const a=3;
if(a>5){
    console.log("a e mai mare ca 5");
}//merge si else if
else{
    console.log("a nu e mai mare ca 5")
}
let b=0;
a>5 ? b=10 : b=-10;
console.log(b);
*/
// var a=3;

// switch(a){
//     case 10:
//         console.log("a e 10");
//         break;
//     case 8:
//         console.log("a e 8");
//         break;
//     default:
//         console.log("e caca");
//         break;
// }

// for(let i=0;i< 10;i++)
// {
//     console.log(i);
// }
// //console.log(i); nu mai merge

// let a=0;
// while(a<10){
//     console.log(a);
//     a=a+2;
// }

// function suma(a, b){
//     return a+b;
// }
// const x = suma(3, 10);
// //suma(3, 20);

// const sumaDoi = (a, b)=> a+b;


// const y = sumaDoi(5,10);
// console.log(x, y);

 //const a =[1,4,2, "chestie", undefined, 10];
// a.include(2);
// console.log(a[a.length-1]);
// a.push("sfarsit");
// console.log(a);
// a.pop();
// console.log(a);

// a.unshift(3, 10);
// console.log(a);
// a.shift();
// console.log(a.slice(2, 4));
//console.log(a);

//mdn array
// const afisareElement = (el, index) => {
//     a[index]=1;
// }

// for(let i=0; i<a.length; i++)
// {
//     console.log(a[i]);
// }
// a.forEach(afisareElement);

// a.forEach((el)=>{
//     console.log(el);
// });

// const b=[1,2,3,4,5];
// // // const c=b.map((el, index) =>(el+3)*index);
// // // console.log("b: ", b);
// // // console.log("c: ", c);
//  const c= b.filter(el => el>3);
//  console.log(b,c);

// const d = [...b, c]; //spread operator -> ia componenetele
// console.log(d); //array in array
// console.log(d[5][0]);

let Tibi = {
    varsta:22,
    anStudiu:3,
    facultate:'CSIE',
    specializare: 'IE',
    hobbyuri: ["programare", "sport"],
    trasaturiFizice: {
        culoareOchi: "caprui",
        par:"saten"
    },
    esteMajor: function(){
        return this.varsta >18;
    }
};
console.log(Tibi.varsta);
console.log(Tibi["anStudiu"]);
console.log(Tibi.esteMajor());

const TibiDoi ={
    ...Tibi,
    note:"7",
    varsta: 25
};

Tibi.trasaturiFizice.culoareOchi="albastrii";
Tibi.varsta =28;
delete Tibi.hobbyuri;
console.log(Tibi, TibiDoi);

